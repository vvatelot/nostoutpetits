var benevoles = [
    'Agnès',
    'Anne-Sylvie',
    'Béatrice ',
    'Carole',
    'Caroline',
    'Chantal',
    'Charlotte',
    'Corinne',
    'Cyril',
    'David',
    'Hélène',
    'Huguette',
    'Isabelle',
    'Laurence',
    'Laurie',
    'Lucile',
    'Marie-Claude',
    'Marie-Bernadette',
    'Maryse',
    'Ophélie',
    'Patricia',
    'Perrine',
    'Pierre',
    'Séverine',
    'Stéphanie',
    'Véronique ',
    'Vincent'
];

function display(name) {
    var police = polices[Math.floor(Math.random() * 3)];
    var id = makeId();
    
    $("<div>", {
        class: "tablecell",
        id: id
    })
            .css({
                fontFamily: police,
                fontSize: '80px'
            })
            .text(name)
            .appendTo("#table")
            .hide()
            .fadeIn(1000)
            .delay(2000)
            .fadeOut(1000);

    setTimeout(function () {
        $("#" + id).remove();
    }, 4000)
}

var n = benevoles.length;

function main() {
    for (i = 0; i <= n; i++) {
        (function (i) {
            setTimeout(function () {
                display(benevoles[i]);
                if (i == n) {
                    main();
                }
            }, 4000 * i);
        })(i)
    }
}

main();


	$(function() {
		cbpBGSlideshow.init();
	});