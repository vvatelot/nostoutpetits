/**
 * @author Vincent Vatelot pour Alice et l'assocation Nos tout petits
 * @since 12/12/2015
 */


$(document).ready(function () {
    var stepH = 25;         // Nbre de positions horizontales.
    var stepV = 15;         // Nbre de positions verticales.
    var minFontSize = 11;   // Taille de police mini.
    var minDelay = 5000;    // Durée mini du délai.
    var positions = [];

    function calcPositions() {
        var width = $(window).width();
        var height = $(window).height();

        for (i = stepV; i > 0; i--) {
            for (j = 0; j < stepH; j++) {
                positions.push([j * (width / (stepH - (i % 2)/2)), i * (height / (stepV - (j % 2)/2))]);
            }
        }
    }


    function generateRandomPosition() {
        var countAvailablePositions = positions.length;
        var pick = Math.floor(Math.random() * countAvailablePositions);
        var result = positions[pick];

        positions.splice(pick, 1);

        return result;
    }


    function displayName(name) {
        var position = generateRandomPosition();
        var delay = minDelay + Math.random() * minDelay * 2;
        var fontSize = minFontSize + Math.random() * minFontSize + "px";
        var id = makeId();
        var police = polices[Math.floor(Math.random() * 3)];

        if (name[0] === 'Alice' || name[0] === 'Léo') {
            fontSize = 2 * minFontSize + "px";
            delay = 3 * minDelay;
        }

        $("<div>", {
            id: id,
            class: "name",
        })
                .css({
                    fontSize: fontSize,
                    position: 'absolute',
                    color: couleurs[name[1]],
                    fontFamily: police
                })
                .offset({
                    top: position[1],
                    left: position[0],
                })
                .text(name[0])
                .appendTo("#cloud-intro")
                .hide()
                .fadeIn(delay)
                .delay(delay)
                .animate({
                    left: '+=10%',
                    opacity: 0
                }, delay * 1.5);

        setTimeout(function () {
            $("#" + id).remove();
            positions.push(position);
            displayName(name);
        }, delay * 3.5)
    }

    function main(move) {
        var nb = prenoms.length;
        calcPositions();

        for (i = 0; i < nb; i++) {
            displayName(prenoms[i]);
        }
    }

    $(window).resize(function () {
        location.reload();
    });

    main();
});
