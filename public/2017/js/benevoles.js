var benevoles = [
    ['Agnès', 1],
    ['Annabelle', 1],
    ['Anne-Sylvie', 1],
    ['Brigitte', 1],
    ['Carole', 1],
    ['Caroline', 1],
    ['Cécile', 1],
    ['Charlotte', 1],
    ['Corinne', 1],
    ['Cyril', 1],
    ['David', 1],
    ['Florence', 1],
    ['Hélène', 1],
    ['Huguette', 1],
    ['Isabelle', 1],
    ['Jean-Paul', 1],
    ['Laurie', 1],
    ['Lucile', 1],
    ['Marie-Claude', 1],
    ['Marie-Bernadette', 1],
    ['Marie-Christine', 1],
    ['Marika', 1],
    ['Maryse', 1],
    ['Mathilde', 1],
    ['Micha', 1],
    ['Ophélie', 1],
    ['Perrine', 1],
    ['Pierre', 1],
    ['Rudy', 1],
    ['Sophie', 1],
    ['Stéphanie', 1],
    ['Véronique', 1],
    ['Vincent', 1],
    ['Alain', 0],
    ['Anaïs', 0],
    ['Axel', 0],
    ['Béatrice', 0],
    ['Chantal', 0],
    ['Denis', 0],
    ['Guillaume', 0],
    ['Grégory', 0],
    ['Laurence', 0],
    ['Michel', 0],
    ['Patricia', 0],
    ['Rachel', 0],
    ['Sylvie', 0]
];

function display(name) {
    var police = polices[Math.floor(Math.random() * 3)];
    var id = makeId();

    if (name[1] == 1) {
        $("h1").text("Merci aux bénévoles de l'association:");
    } else {
        $("h1").text("Merci aux organisateurs de la soirée de Noël:");
    }

    $("<div>", {
        class: "tablecell",
        id: id
    })
        .css({
            fontFamily: police
        })
        .text(name[0])
        .appendTo("#table")
        .hide()
        .fadeIn(1000)
        .delay(2000)
        .fadeOut(1000);

    setTimeout(function () {
        $("#" + id).remove();
    }, 4000)
}

var n = benevoles.length;

function main() {
    for (i = 0; i <= n; i++) {
        (function (i) {
            setTimeout(function () {
                if (i == n) {
                    main();
                } else {
                    display(benevoles[i]);
                }
            }, 4000 * i);
        })(i)
    }
}

main();

$.backstretch([
    "./images/hebus_1920x1200_1481630949_2750.jpg",
    "./images/hebus_1920x1200_1481630963_6199.jpg",
    "./images/hebus_1920x1200_1481630973_9576.jpg",
    "./images/hebus_1920x1200_1481630984_4285.jpg",
    "./images/hebus_1920x1200_1481631006_379.jpg",
    "./images/hebus_1920x1200_1481634734_3859.jpg"
], { duration: 10000, fade: 750 });